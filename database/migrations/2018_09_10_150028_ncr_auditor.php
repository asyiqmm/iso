<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NcrAuditor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ncr_auditor', function (Blueprint $table) {
            $table->increments('id_ncr_auditor');
            $table->string('kategori');
            $table->string('uraian_ktdsn');
            $table->string('bukti_ktdsn');
            $table->integer('auditor_id')->unsigned();
            $table->timestamps();
            $table->foreign('auditor_id')->references('id_auditor')->on('auditor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
