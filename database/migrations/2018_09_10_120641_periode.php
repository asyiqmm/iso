<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Periode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('periode', function (Blueprint $table) {
            $table->increments('periode_id');
            $table->date('tgl_mulai');
            $table->date('tgl_akhir');
            $table->integer('periode_ke');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
