<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Auditor;

class auditorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function auditor_submit(Request $request)
    {
        $req = $request->all();

      $inputStatus = $req['check'];

      for($i = 0; $i < count($inputStatus); $i++) {
        if (!empty($inputStatus[$i])) {
          Auditor::create(array(
            'id_user' => $inputStatus[$i],
            'id_periode' => $req['periode']
          ));
        }
      }
      return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kategori' => 'required',
            'uraian_ktdsn' => 'required',
            'bukti_ktdsn' => 'required',
            'auditor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('auditor/ncr')
                        ->withErrors($validator)
                        ->withInput();
        }
        $insert = NcrAuditor::insert($request->except('_token'));
        if ($insert) {
            return 'Insert berhasil';
        } else {
            return 'Insert gagal';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        // $auditor = Auditor::where('id_auditor','=',$id)->with('usersku')->get();
        // dd($auditor );
        // return view('administrator.indexauditor',compact('auditor'));
        $Auditor = Auditor::with(["periode","user"])
        ->where('id_periode','=',$id)->get();
        //dd($Auditor);
        //return response()->json($Auditor);
        return view('administrator.indexauditor',compact('Auditor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auditor::where('id_auditor',$id)->delete();
        return redirect()->back();
    }

    
}
