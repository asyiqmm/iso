<?php
//halaman pertama kali di akses
Route::get('/',function(){
	if (Auth::guest()) {
		return Redirect::to('auth/login');
	}else{
		if ((Auth::user()->role_id_user)==1) {	
			return Redirect::to('/admin/periode');
		}elseif ((Auth::user()->role_id_user)==2) {
			return Redirect::to('/menu');
		}
	}
})->middleware('auth');
Route::get('/back', function () {
    if (Auth::user()->role_id_user==2) {
		return Redirect::to('/auditor');
	}elseif (Auth::user()->role_id_user==3) {
		return Redirect::to('/auditee');
	}else{
		return Redirect::to('/auth/login');
	}
});

//fitur login
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('/coba', 'Auth\AuthController@viewrole');
Route::get('admin/auditor', 'Auth\AuthController@viewrole');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//halaman admin

Route::get('/admin/auditor',function(){
	return view('administrator.index');
});
//
//periode
Route::get("/admin/periode",function(){
	if (Auth::user()->role_id_user==1) {
		$periode = DB::table('periode')->orderBy('periode_id','ASC')->get();

		return view('administrator.indexperiode', ['periode' => $periode]);
	}else{
		return Redirect::to('/back');
	}

});
Route::get("/admin/auditor",function(){
	if (Auth::user()->role_id_user==1) {
		$periode = DB::table('periode')->orderBy('periode_id','ASC')->get();
		$users = DB::table('users')->orderBy('name','ASC')->get();

		return view('administrator.index', ['users' => $users, 'periode' => $periode]);
	}else{
		return Redirect::to('/back');
	}

});
Route::post('/admin/periode/create','periodeController@store')->name('admin.periode.insert');
Route::post('/admin/auditor/submit','auditorController@auditor_submit');
Route::get('/join','joinController@viewrole');

Route::get('/admin/periode/{id}','periodeController@viewPeriode')->name('admin.periode.view');
 Route::get('/admin/auditor/{id}','auditorController@show')->name('admin.periode.auditor');
 Route::get('/useraudit_delete/{id}','auditorController@destroy');
Route::get('/admin/periode/delete/{id}','periodeController@deletePeriode')->name('admin.periode.delete');


//pemilihan menu untuk role 2
Route::get('menu',function(){
	return view('menu.index');
});


//firstpage of admin
// Route::get("/admin",function(){
// 	if (Auth::user()->role_id_user==1) {
// 		$users = DB::table('users')->where('assigned','=',0)->get();

// 		return view('administrator.index', ['users' => $users]);
// 	}else{
// 		return Redirect::to('/back');
// 	}

// });


//auditor akses
Route::get('/auditor','auditorncrController@view');
Route::get('/auditor/ncr','auditorncrController@show');
Route::post('/auditor/ncr/insert','auditorncrController@store')->name('auditor.ncr.insert');
Route::get('/auditor/ncr/tgl_realisasi','tglrealisasiController@show');

Route::get('/auditor/observation','auditorobservationController@show');
Route::post('/auditor/observation/insert','auditorobservationController@insert');
Route::get('/auditor/observation/realisasi',function(){
	return view('auditor.indexrealisasi');
});

//auditee akses
Route::get('/auditee','auditeencrController@view');
Route::get('/auditee/ncr','auditeencrController@show');
Route::post('/auditee/ncr/insert','auditorncrController@insert');

Route::get('/auditee/observation','auditorobservationController@show');
Route::post('/auditee/observation/insert','auditorobservationController@insert');


// Route::get('/auditor', function () {
// 	if (Auth::user()->role_id_user==2) {
// 		return view('auditor.index');
// 	}else{
// 		echo "anda admin";
// 	}
    
// })->middleware('auth');;

Route::get('admin/ncr', function () {
    return view('administrator.ncr');
})->middleware('auth');;

Route::get('admin/observation', function () {
    return view('administrator.observation');
})->middleware('auth');;

Route::get('admin/ofi', function () {
    return view('administrator.ofi');
})->middleware('auth');;

// Route::get('/auditor_ncr', function () {
//     return view('auditor.index');
// })->middleware('auth');;

// Route::get('/auditor_observation', function () {
//     return view('auditor.observation');
// })->middleware('auth');;


