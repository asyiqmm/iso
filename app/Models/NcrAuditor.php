<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NcrAuditor extends Model
{
    protected $table = 'ncr_auditor';
}
