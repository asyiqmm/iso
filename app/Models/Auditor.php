<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auditor extends Model
{
    protected $table = 'auditor';

    protected $fillable = ['deskripsi','id_user', 'id_periode'];

    public function user(){
        return $this->hasMany('App\User','id','id_user');
    }

    public function periode(){
        return $this->hasOne('App\Models\Periode','periode_id','id_periode');
    }
}

