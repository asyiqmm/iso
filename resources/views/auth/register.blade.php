 <!-- resources/views/auth/register.blade.php -->

    <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet">
@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  @if (count($errors) > 0)
    <div class="alert alert-error">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
<form method="POST" action="/auth/register">
    {!! csrf_field() !!}

    <div>
        Name
        <input type="text" name="name" value="{{ old('name') }}">
    </div>
    <div>
        NIP
        <input type="text" name="nip" value="{{ old('nip') }}">
    </div>

    <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        Password
        <input type="password" name="password">
    </div>

    <div>
        Confirm Password
        <input type="password" name="password_confirmation">
    </div>
    <div>
        role
        <input type="text" name="role_id_user" value="{{old('role_id_user')}}">
    </div>
    
    <div>
        <button type="submit">Register</button>
    </div>
</form>