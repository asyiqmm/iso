<!-- Header -->
@include('header')

<div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">
		<!-- Sidebar -->
		@include('auditor.sidebar')

		<!-- Content -->
		@include('auditor.realisasi')
		</div>
      <div class="display-type"></div>
 </div>
 
<!-- Footer -->
@include('footer')