        <div class="content-w">
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <div class="element-box">
                    <div class="steps-w">
                      <div class="step-triggers">
                        <div class="step-trigger">Tanggal Realisasi Penyelesaian</div>
                      </div>
                        <form action="/auditor/ncr/insert" method="post">
                        
                        {{ csrf_field() }}
                        
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for=""> Tanggal :</label><input class="form-control" type="date" id="start" name="tgl_mulai"
                                      value=""/>
                              </div>
                              
                                <div class="form-group">
                            <form action="#" method="get">
                              <p>Verifikasi :</p>
                              <p><input type="radio" name="Tindakan" value='diterima'/> Diterima</p>
                              <p><input type="radio" name="Tindakan" value='ditolak'/> Ditolak</p>
                            </form>
                          </div>
                          <div class="form-group">
                            <label> Alasan jika ditolak </label><textarea class="form-control" rows="3"></textarea>
                          </div>
                            
                          </div>
                          
                        </div>
                        <div class="form-buttons-w text-right">
                            <input class="btn btn-primary step-trigger-btn" type="submit" name="submit" value='Submit' />
                          </div>
                        
                        </form>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>