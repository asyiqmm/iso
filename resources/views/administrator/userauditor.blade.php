        <div class="content-w">
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      PERIODE AUDIT
                    </h6>
                    <div class="element-box-tp">
                      <form>
                        {{ csrf_field() }}
                        <div class="steps-w">
                          <div class="step-contents">
                            <div class="step-content active" id="stepContent1">
                            <?php $dataSatu = $Auditor[0]->periode;
                            ?>
                          
                              <div class="col-sm-4">
                                <label for=""> Periode ke :</label>
                                {{$dataSatu["periode_ke"]}}
                              </div>
                              <div class="col-sm-4">
                                <label for="">Start :</label>
                                {{$dataSatu["tgl_mulai"]}}
                              </div>
                              <div class="col-sm-4">
                                <label for="">End :</label>
                                {{$dataSatu["tgl_akhir"]}}
                              </div>
                              <div class="controls-above-table">
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                      <div class="table-responsive">
                        <table class="table table-bordered table-lg table-v2 table-striped">
                          <thead>
                            <tr>
                              <th>
                                No
                              </th>
                              <th> 
                                NIP
                              </th>
                              <th class="col-sm-8">
                                Nama
                              </th>
                              <th>
                                Aksi
                              </th>
                              <th>
                                Auditor
                              </th>
                            </tr>
                          </thead>
                          <?php
                          $nomor=1;
                          ?>
                          <tbody>
                          @foreach($Auditor as $user)
                            <tr style="text-align: center;">
                              <td>
                                {{$nomor++}}
                              </td>
                              <td>
                                {{$user->user[0]->nip}}
                              </td>
                              <td>
                                {{$user->user[0]->name}}
                              </td>
                              <td>
                                <a href="{{ url('useraudit_delete/'.$user->id_auditor) }}"  onclick="return confirm('Are you sure?')">Hapus</a>
                              </td>
                              <td>
                              </td>
                            </tr>
                           @endforeach 
                          </tbody>
                        </table>
                      </div>
                      <div class="controls-below-table">
                        <div class="table-records-info">
                          Showing records 1 - 5
                        </div>
                        <div class="table-records-pages">
                          <ul>
                            <li>
                              <a href="#">Previous</a>
                            </li>
                            <li>
                              <a class="current" href="#">1</a>
                            </li>
                            <li>
                              <a href="#">2</a>
                            </li>
                            <li>
                              <a href="#">3</a>
                            </li>
                            <li>
                              <a href="#">4</a>
                            </li>
                            <li>
                              <a href="#">Next</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="form-buttons-w text-right">
                        <button class="btn btn-primary">Selesai</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @section('script')
          <script type="text/javascript">
            let leader = 0;
            $('.leader-switch').on('change', function () {
              
              if ($(this).prop('checked')) {

                  $('.leader-switch').each( function () {
                    console.log(this.checked)
                    if (this.checked == false) {
                      this.disabled = true
                    }                
                  })
              } else {
                 $('.leader-switch').each( function () {
                    this.disabled = false                
                  })
              }
            })

          </script>
        @endsection