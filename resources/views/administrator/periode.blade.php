        <div class="content-w">
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      PERIODE AUDIT
                    </h6>
                    <div class="element-box-tp">
                      @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif
                      <form action="{{ route('admin.periode.insert')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="steps-w">
                          <div class="step-contents">
                            <div class="step-content active" id="stepContent1">
                              <div class="col-sm-4">
                                <label for=""> Periode Ke :</label>
                                <input class="form-control" name="periode_ke"></input>
                              </div>
                              <div class="col-sm-4">
                                <fieldset>
                                    <div>
                                      <label for="start">Start</label>
                                      <input class="form-control" type="date" id="start" name="tgl_mulai"
                                      value=""/>
                                      <label for="end">End</label>
                                      <input class="form-control" type="date" id="end" name="tgl_akhir"
                                      value=""/>
                                    </div>
                                  </fieldset>

                                  <div class="form-buttons-w text-right">
                                    <button class="btn btn-primary" type="submit">Selesai</button>
                                  </div>
                              </div>
                              <div class="controls-above-table">
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                      <div class="table-responsive">
                        <table class="table table-bordered table-lg table-v2 table-striped">
                          <thead>
                            <tr>
                              <th>
                                ID
                              </th>
                              <th> 
                                Periode Ke
                              </th>
                              <th class="col-sm-3">
                                Tanggal Mulai
                              </th>
                              <th class="col-sm-3">
                                Tanggal Akhir
                              </th>
                              <th>
                                Read
                              </th>
                              <th>
                                Hapus
                              </th>
                               
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $nomor = 1;
                            ?>
                            @foreach($periode as $key => $data)
                            <tr style="text-align: center;">
                              <td>
                                {{ $nomor++ }}
                              </td>
                              <td>
                                {{ $data->periode_ke }}  
                              </td>
                              <td>
                                {{ $data->tgl_mulai }}
                              </td>
                              <td>
                                {{ $data->tgl_akhir }}
                              </td>
                              <td>
                                <a href="{{route('admin.periode.auditor', $data->periode_id)}}">>></a>
<!--                                 <label class="switch">
                                  <input type="checkbox" value="1" name="leader" class="leader-switch">
                                  <span class="slider round"></span>
                                </label> -->
                              </td>
                              <td>
                                <a href="{{route('admin.periode.delete', $data->periode_id)}}">hapus</a>
                              </td>
                              <!-- <td>
                                <label class="switch">
                                  <input type="checkbox" name="">
                                  <span class="slider round"></span>
                                </label>
                              </td> -->
                            </tr>
                            @endforeach
                            
                          </tbody>
                        </table>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        @section('script')
          <script type="text/javascript">
            let leader = 0;
            $('.leader-switch').on('change', function () {
              
              if ($(this).prop('checked')) {

                  $('.leader-switch').each( function () {
                    console.log(this.checked)
                    if (this.checked == false) {
                      this.disabled = true
                    }                
                  })
              } else {
                 $('.leader-switch').each( function () {
                    this.disabled = false                
                  })
              }
            })

          </script>
        @endsection