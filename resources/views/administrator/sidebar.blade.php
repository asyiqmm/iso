        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
          <div class="mm-logo-buttons-w">
            <a class="mm-logo" href="index.html"><img src="img/logo.png"><span>PDAM SURYA SEMBADA</span></a>
            <div class="mm-buttons">
              <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
              </div>
              <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="desktop-menu menu-side-w menu-activated-on-click">
          <div class="logo-w">
            <a class="logo" href="{{ url('/') }}"><img src="img/logo.png"></a>
          </div>
          <div class="menu-and-user">
            <div class="logged-user-w">
              <div class="avatar-w">
                <img alt="" src="img/avatar1.jpg">
              </div>
              <div class="logged-user-info-w">
                <div class="logged-user-name">
                  {{Auth::user()->name}}
                </div>
                <div class="logged-user-role">
                  Administrator
                </div>
              </div>
              <div class="logged-user-menu">
                <div class="avatar-w">
                  <img alt="" src="img/avatar1.jpg">
                </div>
                <div class="logged-user-info-w">
                  <div class="logged-user-name">
                    {{Auth::user()->name}}
                  </div>
                  <div class="logged-user-role">
                    Administrator
                  </div>
                </div>
                <div class="bg-icon">
                  <i class="os-icon os-icon-wallet-loaded"></i>
                </div>
                <ul>
                  <li>
                    <a href="/auth/logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                  </li>
                </ul>
              </div>
            </div>
            <ul class="main-menu">
              <li class="has-sub-menu">
                <a href="index.html">
                  <div class="icon-w">
                    <div class="os-icon os-icon-window-content"></div>
                  </div>
                  <span>Audit Internal</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="{{ url('admin/ncr') }}">NCR</a>
                  </li>
                  <li>
                    <a href="{{ url('admin/observation') }}">Observasion</a>
                  </li>
                  <li>
                    <a href="{{ url('admin/ofi') }}">OFI</a>
                  </li>
                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="index.html">
                  <div class="icon-w">
                    <div class="os-icon os-icon-window-content"></div>
                  </div>
                  <span>Audit Internal</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="/admin/periode">periode</a>
                  </li>
                  <li>
                    <a href="/admin/auditor">auditor</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>